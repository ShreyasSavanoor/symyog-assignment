import { TestBed } from '@angular/core/testing';

import { ProfieDataService } from './profie-data.service';

describe('ProfieDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfieDataService = TestBed.get(ProfieDataService);
    expect(service).toBeTruthy();
  });
});
