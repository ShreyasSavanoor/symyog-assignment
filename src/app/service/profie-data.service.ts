import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProfieDataService {
  private url = '../../assets/data/';
  profileData;
  alphaTestProfile = [];
  constructor(private http: HttpClient) { }

  public getProfileData() {
    return this.http.get(this.url + 'input.json');
  }

  public processProfileData(data) {
    this.alphaTestProfile = [];
    this.profileData = data['Performance Test Profile'];
    this.profileData.forEach(pData => {
      this.alphaTestProfile.push(pData['AlphaTest Profile']);
    });
    this.alphaTestProfile = _.orderBy(this.alphaTestProfile, ['Total AlphaTest Time'], ['desc']);
    return this.alphaTestProfile;
  }
}
