import { Component, OnInit } from '@angular/core';
import { ProfieDataService } from './service/profie-data.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'symyog';

  constructor() { }

  ngOnInit() {
    
  }
}
