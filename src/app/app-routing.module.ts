import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterViewComponent } from '../app/shared/master-view/master-view.component';
import { DetailViewComponent } from '../app/shared/detail-view/detail-view.component';

const routes: Routes = [
  {
    path: '',
    component: MasterViewComponent
  },
  {
    path: 'details',
    component: DetailViewComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
