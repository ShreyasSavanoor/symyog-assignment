import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfieDataService } from '../../service/profie-data.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit {
  index;
  alphaTestProfile = [];
  layerData = [];
  headers: Array<any> = [
    {
      label: 'Layer Name',
      key: 'Layer Name'
    },
    {
      label: 'Layer Effective AlphaTest Time',
      key: 'Layer Effective AlphaTest Time'
    },
    {
      label: 'Layer AlphaTest Time',
      key: 'Layer AlphaTest Time'
    },
    {
      label: 'Layer Step1 Time',
      key: 'Layer Step1 Time'
    },
    {
      label: 'Layer Step2 Time',
      key: 'Layer Step2 Time'
    }
  ];
  @Input() data;

  constructor(public profiledataservice: ProfieDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.index = params.index;
      this.fetchData(this.index);
    });
  }

  fetchData(index) {
    this.profiledataservice.getProfileData().subscribe(data => {
      this.alphaTestProfile =  this.profiledataservice.processProfileData(data);
      this.layerData = this.alphaTestProfile[index]['Layer Info'];
    });
  }

}
