import { Component, OnInit } from '@angular/core';
import { ProfieDataService } from '../../service/profie-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-master-view',
  templateUrl: './master-view.component.html',
  styleUrls: ['./master-view.component.scss']
})
export class MasterViewComponent implements OnInit {
  alphaTestProfile = [];
  headers: Array<any> = [
    {
      label: 'Result',
      key: 'Result',
      status: true
    },
    {
      label: 'Design Name',
      key: 'Design Name'
    },
    {
      label: 'Step1 Time',
      key: 'Step1 Time'
    },
    {
      label: 'Step2 Time',
      key: 'Step2 Time'
    },
    {
      label: 'Step3 Time',
      key: 'Step3 Time'
    },
    {
      label: 'Total AlphaTest Time',
      key: 'Total AlphaTest Time'
    },
  ];
  constructor(public profiledataservice: ProfieDataService, public router: Router) { }

  ngOnInit() {
    this.profiledataservice.getProfileData().subscribe(data => {
      this.alphaTestProfile =  this.profiledataservice.processProfileData(data);
    });
  }

  detailView(index) {
    this.router.navigate(['details'], { queryParams: { index } });
  }

}
