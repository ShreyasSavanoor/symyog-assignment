import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() height;
  @Input() width;
  @Input() tableWidth;
  @Input() headPadding;
  @Input() dataPadding;
  @Input() headBackground;
  @Input() headColor;
  @Input() alternateBackground;
  @Input() alternateColor;
  @Input() head: Array<any> = [];
  @Input() data: Array<any> = [];
  @Output() clickValue = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  rowClick(index) {
    this.clickValue.emit(index);
  }

  showStatus(data) {
    if (data.Result === 'FAIL') {
      return true;
    }
  }

}
